import re
import csv
import pandas
import openpyxl
import matplotlib.pyplot as mp
import sys
import os 

# ---------------------------------------------------------------
#  remove unwanted characters
def replace_brackets_braces(c)
    if [ in c
        c = c.replace([, )
    if ] in c
        c = c.replace(], )
    if { in c
        c = c.replace({, )
    if } in c
        c = c.replace(}, )
    return c

def replace_uneeded_chars(c)
    print(c)
    if ',' in c
        print('hi')
        c = c.replace(,, )
        return c
    if F in c
        c = c.replace(F, )
        return c
    if f in c
        c = c.replace(f, )
        return c
    if # in c
        c = c.replace(#, )
        return c


def remove_unwanted_char_from_list(l, c)
    for y in l
        if c in y
            # print('y', y)
            y2 = y.replace(c, '')
            ind = l.index(y)
            l[ind] = y2
    return l

def remove_convert_hex(c)
    if 0x in c
        c = c.replace(0x, )
        return str(int(c, base=16))
# returns array of column ids
def get_column_ids(l)
    for i in l
        n = i.split(=)
        if len(n)  2
            column_id.append(n[0] + n[1])
        else
            column_id.append(n[0])

def get_label_value(l)
    for i in l
        n = i.split(=)
        row_list.append(n[-1])

def find_date_and_file(i)
    find_date = re.search(d{4}-d{2}-d{2} d{2}d{2}d{2}.d{6}, i)
    find_file = re.search(r'(!-o)s([^-]+.[c]+)', i)
    if find_file
        # print(find_file)
        trace_file = i[find_file.span()[0]find_file.span()[1]]
        trace_file = i[find_file.span()[0]find_file.span()[1]].split( )[-1]
    if find_date
        date_match = i[find_date.span()[0]find_date.span()[1]]
        # print(date_match)
        if len(column_id) == 0
            column_id.insert(0, timestamp)
        elif column_id[0] != timestamp
            column_id.insert(0, timestamp)
        row_list.insert(0, date_match)
    total_rows.append(row_list)

trace_path = sys.argv[1]

# ---------------------------------------------------------------
# get command line arguements to use for trace list
n = len(sys.argv)
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# USAGE 
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


trace_list = sys.argv[2]
# ---------------------------------------------------------------
# list to return all sub lists to create the dataframe

# ---------------------------------------------------------------
# tmp list used to store numeric values of statistic data and appended to whole list
row_list = []
total_rows = []

# ---------------------------------------------------------------
# store labels of stats
column_id = []

# ---------------------------------------------------------------
# loop through user values wanted
for tag in trace_list
    f = open(trace_path)
    # ---------------------------------------------------------------
    # main for loop to parse trace line
    for i in f
        # ---------------------------------------------------------------
        # match the trace you are looking for
        if re.search(tag, i)
            # ---------------------------------------------------------------
            # match the trace you are looking for
            if 'VAL1' in i or 'VAL2' in i or 'VAL3' in i
                if 'VAL1' in i
                    l = i.split('VAL1')[1]
                    l = l.split(' ')
                    l = [ x for x in l if = in x ]
                    l = remove_unwanted_char_from_list(l, ,)
                    l = remove_unwanted_char_from_list(l, {)
                    l = remove_unwanted_char_from_list(l, })
                    if len(column_id) == 0
                        get_column_ids(l)

                    get_label_value(l)
                    find_date_and_file(i)
                    total_rows.append(row_list)
                    row_list = []
                elif 'VAL2' in i
                    l = i.split('VAL2')[1]
                    l = l.split(' ')
                    l = [ x for x in l if = in x ]
                    l = remove_unwanted_char_from_list(l, ,)
                    l = remove_unwanted_char_from_list(l, {)
                    l = remove_unwanted_char_from_list(l, })
                    if len(column_id) == 0
                        get_column_ids(l)

                    get_label_value(l)
                    find_date_and_file(i)
                    total_rows.append(row_list)
                    print('l', l)
                    print('row', row_list)
                    row_list = []
                elif 'VAL3' in i
                    l = i.split('VAL3')[1]
                    l = l.split(' ')
                    l = [ x for x in l if = in x ]
                    l = remove_unwanted_char_from_list(l, ,)
                    l = remove_unwanted_char_from_list(l, {)
                    l = remove_unwanted_char_from_list(l, })
                    if len(column_id) == 0
                        get_column_ids(l)

                    get_label_value(l)
                    find_date_and_file(i)
                    total_rows.append(row_list)
                    row_list = []


if len(total_rows) > 0:
    save_dir_parse = sys.argv[0].split('')
    save_dir_path =  + save_dir_parse[1] +  + save_dir_parse[2] +  + save_dir_parse[3]
    save_file_xl = 'xlsx' + tag + '.xlsx'
    save_file_csv = 'csv' + tag + '.csv'
    df = pandas.DataFrame(total_rows, columns=column_id)
    print('saving ', save_dir_path + '' + save_file_csv)
    print('saving ', save_dir_path + '' + save_file_xl)
    writer = pandas.ExcelWriter(save_dir_path + '' + save_file_xl, engine='openpyxl')
    df.to_excel(writer, sheet_name='Sheet1')
    df.to_csv(save_dir_path + '' + save_file_csv)  
    writer.save()
    total_rows = []
column_id = []
f.close()

